Kingdom Desktop -
-------
Aplicación de escritorio para Windows, Mac y Linux de Kingdom of Words.  
Hemos creado la Desktop App de Windows, Mac y Linux para que puedas disfrutar de nuestra web sin tener que acceder a navegadores y llevártelo con un pen-drive.  
```ruby
Totalmente compatible con cualquier dispositivo hasta con Raspberry.  
```
Descargas: 
* [Windows](https://github.com/semoal/KingdomDesktop/releases/download/v1.0/KingdomDesktop-win32-x64.zip) -- `174 MB KingdomDesktop-win32-x64.zip`
* [Mac](https://github.com/semoal/KingdomDesktop/releases/download/v1.0/KingdomDesktop-mac-x64.zip) -- `39.6 MB KingdomDesktop-mac-x64.zip`
* [Linux](https://github.com/semoal/KingdomDesktop/releases/download/v1.0/KingdomDesktop-linux-x64.zip) -- `41.9 MB KingdomDesktop-linux-x64.zip`


👍 Cualquier sugerencia contactar a: welcome.kingdomwords@gmail.com
